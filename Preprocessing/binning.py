import numpy as np
import pandas as pd
import matplotlib as plt
from matplotlib import pyplot

def group(df, column_name, number_of_bin):
    df[column_name] = df[column_name].astype(float, copy=True)
    print("Sum: ", df[column_name].sum(), " Max : ", max(df[column_name]), " Min: ", min(df[column_name]))
    binwidth = (max(df[column_name]) - min(df[column_name])) / number_of_bin
    print("bindwidth:", binwidth)
    bins = np.arange(min(df[column_name]), max(df[column_name]), binwidth)
    print("Bins: ", bins)
    group_names = ['Low', 'Medium', 'High']
    df[column_name + '-binned'] = pd.cut(df[column_name], bins, labels=group_names, include_lowest=True)
    print(df[[column_name, column_name + '-binned']].head(20))
    return df;

def bin_visual(df, column_name, no_of_bin, x_label, y_label, title):
    a = (0, 1, 2)
    # draw historgram of attribute "horsepower" with bins = 3
    plt.pyplot.hist(df[column_name], bins=no_of_bin)
    # set x/y labels and plot title
    plt.pyplot.xlabel(x_label)
    plt.pyplot.ylabel(y_label)
    plt.pyplot.title(title)
    plt.show()
    return
