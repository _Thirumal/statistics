import numpy as np

def print_missing_column(missing_data):
    print("Missing column")
    for column in missing_data.columns.values.tolist():
        print(column)
        print("Missing date count: ", missing_data[column].value_counts())
        print("")

def replace_with_mean(dataframe, column_name):
    avg = dataframe[column_name].astype("float").mean(axis=0)
    print("Mean before replacing missing value: ", avg)
    dataframe[column_name].replace(np.nan, avg, inplace=True)
    avg = dataframe[column_name].astype("float").mean(axis=0)
    print("Mean after replacing missing value: ", avg)
    return dataframe