import pandas as pd

data = [120.99465111, 109.11708357,  99.34774273,  89.41343927,  91.18446861,
 102.63455293, 112.65371268, 111.88835287, 100.19060694,  90.95766396,
  94.7211922,  110.70925754, 119.59678354, 109.67060177, 100.60720933,
  89.24256942,  91.16272296, 102.11967296, 111.78687317, 110.52226665]

data_index = pd.read_csv('../dataset/Electric_Production.csv',index_col=0)
test = data_index.loc['2017-01-01':]
print(test.index)
future_forecast = pd.DataFrame(data, index =test.index, columns=['Prediction'])
#
print(future_forecast.columns.values.tolist())