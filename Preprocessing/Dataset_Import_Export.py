import pandas as pd
import numpy as np

import missing_data as md
import binning as bn

import Visualization.plot_bin as pl

url = "https://archive.ics.uci.edu/ml/machine-learning-databases/autos/imports-85.data"
path = "../dataset/automobile.csv"

headers = ["symboling","normalized-losses","make","fuel-type","aspiration", "num-of-doors","body-style",
         "drive-wheels","engine-location","wheel-base", "length","width","height","curb-weight","engine-type",
         "num-of-cylinders", "engine-size","fuel-system","bore","stroke","compression-ratio","horsepower",
         "peak-rpm","city-mpg","highway-mpg","price"]
# Read from file
try:
    df = pd.read_csv(path, header=None)
    print("From hard disk")
except FileNotFoundError: # get from url
    df = pd.read_csv(url, header = None)
    # store in file system
    #df.to_csv(path)

# print from top
print("----------------------------")
print("Data from head without header", df.head(2))
# Assign header
df.columns = headers
print("Fetching data from the given url/disk is Done")
# print from top
print("----------------------------")
print("Data from head", df.head(2))
print("----------------------------")
# print from tail
print("Data From tail", df.tail(2))
print("----------data types------------------")
# Check data types
print(df.dtypes)
print("------------summary description----------------")
# summary
print(df.describe(include="all"))
print("------------info----------------")
# full info
print(df.info())
##---------------------Data Wrangling ---------------######
print("------------? replace with nan----------------")
# replace ? to NAN
replaced_head = df.replace("?", np.nan, inplace = True)
print(replaced_head)
print("------------missing data----------------")
# missing data
missing_data = df.isnull()
print("Mising data (Boolean : true represents missing): ", missing_data)

print("----------missing value on each column------")
print(md.print_missing_column(missing_data))
print("------------Dropped nan----------------")
# drop missing value of price
dropped_df = df.dropna(subset=["price"],  axis=0, inplace=True)
print(dropped_df)
print("------------Reset index----------------")
# reset index, because we droped few rows in price
df.reset_index(drop = True, inplace = True)

print("------------Change data type of column----------------")
# Change data type of column
print("Original data type of price:  ", df["price"].dtype)

df[["price"]] = df[["price"]].astype("float")
print("After data type of Price change: ", df["price"].dtype)

print("------------Replace missing data with mean----------------")
# replace with mean
md.replace_with_mean(df, "normalized-losses")

print("------------Validate column with missing values----------")

missing_data1 = df.isna()
print("Missing data 1: ", missing_data1["normalized-losses"].values.tolist())
print("---------------Normalize in between (0 - 1) -------------")
# Normalize the value from 0 to 1
df['length'] = df['length']/df['length'].max()
print(df['length'])
df['width'] = df['width']/df['width'].max()
print(df['width'])

print("#####################Binning########################")
# Binning
print("Binning")
# Clean the data
md.replace_with_mean(df, "horsepower")
df["horsepower"] = df["horsepower"].astype("float", copy=True)

bin_width = (max(df["horsepower"]) - min(df["horsepower"])) / 4

print("Bin Width : ", bin_width)

# Cutting data

bins = np.arange(min(df["horsepower"]), max(df["horsepower"]), bin_width)

print("Bins : ", bins)

# Assign name for bins
group_names = ['Low', 'Medium', 'High']

df['horsepower-binned'] = pd.cut(df['horsepower'], bins, labels=group_names,include_lowest=True )
df[['horsepower','horsepower-binned']].head(20)

#bn.group(df, "horsepower", 4)

# Binning Visual
#bn.bin_visual(df, "horsepower", 3, "horsepower", "count", "horsepower bins")
pl.draw(df["horsepower"])

###########Indicator variable##########

#one - hot encoding
print("One hot encoding")
dummy_variable_1 = pd.get_dummies(df["fuel-type"])
print("Dummy", dummy_variable_1.head())

dummy_variable_1.rename(columns={'fuel-type-diesel':'gas', 'fuel-type-diesel':'diesel'}, inplace=True)
print("Rename dummy", dummy_variable_1.head())

# 0 for Gas & 1 for diesel
# merge data frame "df" and "dummy_variable_1"
df = pd.concat([df, dummy_variable_1], axis=1)

# drop original column "fuel-type" from "df"
df.drop("fuel-type", axis = 1, inplace=True)

print(df.head())