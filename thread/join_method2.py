import threading

import time
import datetime

t1 = datetime.datetime.now()

list1 = []

def fun1(a):
  time.sleep(1)# complex calculation takes 1 seconds
  list1.append(a)

for each in range(10):
  thread1 = threading.Thread(target = fun1, args = (each, ))
  thread1.start()

thread1.join()

print("List1 is: ", list1)

t2 = datetime.datetime.now()

print("Time taken", t2 - t1)