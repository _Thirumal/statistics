import pandas as pd
import datetime
from matplotlib import pyplot
from statsmodels.graphics.tsaplots import plot_acf
from statsmodels.graphics.tsaplots import plot_pacf
from pyramid.arima import auto_arima
from pandas.plotting import autocorrelation_plot

import Preprocessing.missing_data as md

df = pd.read_csv("../dataset/Khanapur_Flows_1981-2001.csv")

#print("Data type: ", df.dtypes)

#print("Missing Data: ", md.print_missing_column(df))


dischargeDate = []
data = []

for index, row in df.iterrows():
    dischargeDate.append(datetime.date(int(row['Year']), int(row['Month']),  int(row['Date'])))
    data.append(row['Discharge'])
print(dischargeDate)

print("New Dataframe with date format")
#clean_df = pd.DataFrame(data, index=dischargeDate, columns=['Discharge'])
clean_df = pd.DataFrame({'Date': dischargeDate, 'Discharge': data})

#headers = ['Date', 'Discharge']
#clean_df.columns = headers
#clean_df = clean_df.rename = ['Date', 'Discharge']

#clean_df.index.name = 'Date'
#clean_df.rename_axis('Date', axis='rows', inplace=True)
print("Columns: ", clean_df.columns.values.tolist())
clean_df['Date'] = pd.to_datetime(clean_df['Date'])



#print("clean df", clean_df)
#df["Date"] = df["date"].astype("date", copy=True)
#print("Clean data frame \n", clean_df.dtypes)

print("Date to Monthly Discharge ")
#df['month_year'] = clean_df.date_column.dt.to_period('M')
monthly_discharge = clean_df.resample('M', on='Date').mean()

print(monthly_discharge)



#for column in monthly_discharge:
#    print(df[column])


#for index, row in monthly_discharge.iterrows():
#    print(index, " ", row['Discharge'])



pyplot.plot(monthly_discharge)
pyplot.show()
print("------------Auto-correlation coefficient-----------")
autocorrelation_plot(monthly_discharge)
pyplot.show()

print("------------ACF-----------")
plot_acf(monthly_discharge, lags=15)
pyplot.show()

print("------------PACF-----------")
plot_pacf(monthly_discharge, lags=15)
pyplot.show()

stepwise_model = auto_arima(monthly_discharge, start_p=1, start_q=1,
                           max_p=3, max_q=3, m=12,
                           start_P=0, seasonal=True,
                           d=1, D=1, trace=True,
                           error_action='ignore',
                           suppress_warnings=True,
                           stepwise=True)

print("AIC: ", stepwise_model.aic())

train = monthly_discharge.loc['1981-01-01':'1997-12-01']
test = monthly_discharge.loc['1998-01-01':]

print("-------------Train the model-----------")

print(stepwise_model.fit(train))

print("-------------Evaluation--------------------")
future_forecast = stepwise_model.predict(n_periods=53)
print("Future forecast: ", future_forecast)

print("---------Plot future forecast------------")

future_forecast = pd.DataFrame(future_forecast, index = test.index, columns=['Prediction'])

print("future forecast data frame head: \n", future_forecast.head())

print("concat \n: ", pd.concat([test, future_forecast],axis=1))

pyplot.plot(test, color="blue", label ="test")
pyplot.plot(future_forecast, color ="yellow", label ="forecast")
pyplot.show()

future_forecast2 = future_forecast

pyplot.plot(monthly_discharge, color = "blue", label="original")
pyplot.plot(future_forecast2, color = "red", label="forecast")
pyplot.show()
