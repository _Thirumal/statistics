import pandas as pd
import numpy as np
import matplotlib.pylab as plt
from datetime import datetime
import test_stationarity as is_stationary

from matplotlib.pylab import rcParams

rcParams['figure.figsize'] = 15, 6

data = pd.read_csv('../dataset/AirPassengers.csv')

print(data.head())
print ('Data Types:\n', data.dtypes)

## Preprocessing ##
print("--------Preprocessing--------")
dateparse = lambda dates: pd.datetime.strptime(dates, '%Y-%m')
data = pd.read_csv('../dataset/AirPassengers.csv', parse_dates=['Month'], index_col='Month',date_parser=dateparse)
print(data.head())
print ('Data Index:\n', data.index)

ts = data['#Passengers']
print("ts: \n", ts.head(10))


print("Display data of [1, 1, 1949] :", ts[datetime(1949,1,1)])

# Check the time series is stationary
plt.plot(ts)
plt.show()

print ("------Check the data is stationary-----------")
is_stationary.test_stationarity(ts)

print("--------Remove tread-------------")

print("Log Transformation")
ts_log = np.log(ts)
plt.title("Transformation")
plt.plot(ts_log, color="orange", label="Log")

print("Smoothing: Moving average")
moving_avg = ts_log.rolling(12).mean()
plt.plot(moving_avg, color = "Red", label="Moving Average")
plt.show()

ts_log_moving_avg_diff = ts_log - moving_avg
print("Moving average of 12 value contains only one value: \n", ts_log_moving_avg_diff.head(12))
ts_log_moving_avg_diff.dropna(inplace=True)
is_stationary.test_stationarity(ts_log_moving_avg_diff)

print("Exponential Weighted moving average")
expwighted_avg = ts_log.ewm(halflife=12)
print(expwighted_avg)
plt.plot(ts_log)
plt.plot(expwighted_avg, color='red')
plt.show()