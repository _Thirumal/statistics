import pandas as pd
from matplotlib import pyplot as plt
from matplotlib import style
import plotly
from plotly.plotly import plot_mpl
from statsmodels.tsa.seasonal import seasonal_decompose
from pyramid.arima import auto_arima
# If pyramid in not installed automatically use the command : pip install --upgrade git+https://github.com/tgsmith61591/pyramid.git

data = pd.read_csv('../dataset/Electric_Production.csv',index_col=0)

print("------------Head---------------")
print(data.head())

print("----------Describe--------------")
print(data.describe())

print("--------Cleaning check for NULL-------------------")
print(data[pd.isnull(data['IPG2211A2N'])])

print("--------Index-----------")
print(data.index)

print("-----Data types-------")
print(data.dtypes)

print("----------Change index data type to time stamp")
data.index = pd.to_datetime(data.index)
print(data.index)

print("----------Rename column---------------")
data.columns = ['Energy Production']
print(data.dtypes)

print("-----------Plot---------------------")


style.use("ggplot")
plt.plot(data.index, data["Energy Production"], label="lhello", linewidth=5)
plt.show()
#
#
'''
plotly.tools.set_credentials_file(username='thirumal', api_key='CBwkstftsEXPKk1f95h9')
#
result = seasonal_decompose(data, model='multiplicative')
fig = result.plot()
plot_mpl(fig)
'''
# Choose P, D, Q
# Different ways Auto-Correlation Function (ACF), domain experience ...
# Grid search over multiple value of P, D, Q
''' 
AIC (Alkaike Information Criteria) is an estimator of relative quality of statistical model of given data set.
The AIC value will allow us to compare how well a model fits the data and takes into account the complexity of a model,
so models that have a better fit while using fewer features will receive a better (lower) AIC score than similar models 
that utilize more features
BIC (Bayesian Information Criterion)
'''
stepwise_model = auto_arima(data, start_p=1, start_q=1,
                           max_p=3, max_q=3, m=12,
                           start_P=0, seasonal=True,
                           d=1, D=1, trace=True,
                           error_action='ignore',
                           suppress_warnings=True,
                           stepwise=True)

print("AIC: ", stepwise_model.aic())

print("------------Split data to Train & split------------------")

train = data.loc['1985-01-01':'2016-12-01']
test = data.loc['2017-01-01':]

print("test :\n", test.head())

print("-------------Train the model-----------")

print(stepwise_model.fit(train))

print("-------------Evaluation--------------------")
future_forecast = stepwise_model.predict(n_periods=20)
print("Future forecast: ", future_forecast)

print("---------Plot future forecast------------")

future_forecast = pd.DataFrame(future_forecast, index = test.index, columns=['Prediction'])

print("future forecast data frame head: \n", future_forecast.head())

print("concat \n: ", pd.concat([test, future_forecast],axis=1))

plt.plot(test, color="blue", label ="test")
plt.plot(future_forecast, color ="yellow", label ="forecast")
plt.show()

future_forecast2 = future_forecast

plt.plot(data, color = "blue", label="original")
plt.plot(future_forecast2, color = "red", label="forecast")
plt.show()
