import pandas as pd

def download_dataset(url):
    df = pd.read_csv(url)
    print("Lisitng top 10 data set: ", df.head(10))
    return  df

def download_save(url, file_path):
    try:
        df = pd.read_csv(file_path)
        return df
    except(FileNotFoundError):
        df = pd.read_csv(url);
        df.to_csv(file_path)
        print("Download and saved sucessfully")
        return download_dataset(file_path)