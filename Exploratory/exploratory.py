import pandas as pd

import matplotlib.pyplot as plt
import seaborn as sns

import csv_data as cd

url = "https://ibm.box.com/shared/static/q6iiqb1pd7wo8r3q28jvgsrprzezjqk3.csv"

# Download CSV
#df = cd.download_dataset(url)


# Download and Save

df = cd.download_save(url, "../dataset/exploratory.csv")

print("Data types of peak-rpm: ", df["peak-rpm"].dtypes)

print(df.head())

# Co-relation
print("___________Correlation_______________")
print(df.corr())

print("----------------Positive Linear Regression -------------------")
# Engine size as potential predictor variable of price
sns.regplot(x="engine-size", y="price", data=df)
plt.ylim(0,)
plt.show()


print("Co-relation between engin-size & price: ", df[["engine-size", "price"]].corr())


print("----------------Negative Linear Regression -------------------")

sns.regplot(x="highway-mpg", y="price", data=df)
plt.show()

print("Co-relation between highway-mpg & price: ", df[["highway-mpg", "price"]].corr())

print("----------------Weaker Linear Regression -------------------")

sns.regplot(x="peak-rpm", y="price", data=df)
plt.show()

print("Co-relation between peak-rpm & price: ", df[["peak-rpm", "price"]].corr())


### Categorical variable (Box plot)
sns.boxplot(x="body-style", y="price", data=df)
plt.show()
sns.boxplot(x="engine-location", y="price", data=df)
plt.show()
# drive-wheels
sns.boxplot(x="drive-wheels", y="price", data=df)
plt.show()