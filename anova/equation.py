import numpy as np


m = np.array([[7260, 120], [583220, 7260]])
print(m)
c = np.array([3514705, 317468398.6])
print(c)
y = np.linalg.solve(m, c)
print("Y values are : ", y)


m = np.array([[583220, 7260, 120], [52707600, 583220, 7260], [5080895996, 52707600, 583220]])
print(m)
c = np.array([3514705, 317468398.6, 30596302179])
print(c)
y = np.linalg.solve(m, c)
print("Y values are : ", y)


m = np.array([[385, 55, 10], [3025, 385, 55], [25333, 3025, 385]])
print(m)
c = np.array([2628.1, 20158.78, 166767.08])
print(c)
y = np.linalg.solve(m, c)
print(y)
