from matplotlib import pyplot as plt
from matplotlib import style

style.use("ggplot")
plt.plot([1, 2, 3], [4, 5, 1], 'b', label="line 1", linewidth=5)
plt.title("learning plot")
plt.xlabel("X-axis")
plt.ylabel("Y-axis")

#
x = [3, 5, 7]
y = [5, 9, 3]
plt.plot(x, y, 'g', label="line 2", linewidth=5)

plt.legend()

plt.grid(True, color="y")

plt.show()