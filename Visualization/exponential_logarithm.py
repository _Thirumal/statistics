import math
from matplotlib import pyplot as plt
import pandas as pd

e = []
for i in range(1, 6):
    e.append(2**i)
print("Exponential of 2 ", e)
#e = pd.DataFrame(e, index=range(1, 10))

l = []
for i in range(1, 6):
    l.append(math.log(i, 2))

print("Logarithm of 2: ", l)
l# = pd.DataFrame(l, index=range(1, 10))

plt.plot(e, color = "black", label="exponential")
plt.plot(l, color = "green", label="logarithm")
plt.show()
