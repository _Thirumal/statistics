import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.linear_model import LinearRegression


# print all output
desired_width = 500
pd.set_option('display.width', desired_width)
np.set_printoptions(linewidth=desired_width)

pd.set_option('display.max_columns',100)

path = 'https://ibm.box.com/shared/static/q6iiqb1pd7wo8r3q28jvgsrprzezjqk3.csv'
df = pd.read_csv(path)
print(df.head())

# Linear regression
print("==================== Linear Regression =================")
lm = LinearRegression()
print(lm)

X = df[["highway-mpg"]]
Y = df[["price"]]

# Fit linear model
lm.fit(X, Y)

# predict output
Yhat = lm.predict(X)
print("predicted Y value: ", Yhat[0:10])

# Find Intercept
print("Intercept: ", lm.intercept_)

# slope (Co-efficient)
print("Slop (Intercept): ", lm.coef_)


# $$$$ Multiple Linear Regression $$$$$$#
print("==================== Multiple Linear Regression =================")
Z = df[['horsepower', 'curb-weight', 'engine-size', 'highway-mpg']]

# Fit Multiple linear model
lm.fit(Z, Y)

# intercept
print("Intercept in multi linear Regression : ", lm.intercept_)

# Slope
print("Slope (Co-efficient: ", lm.coef_)


# Model evaluation using visualization
width = 12
height = 10
plt.figure(figsize=(width, height))
sns.regplot(x="highway-mpg", y="price", data=df)
plt.ylim(0,)
plt.show()

plt.figure(figsize=(width, height))
sns.regplot(x="peak-rpm", y="price", data=df)
plt.ylim(0,)
plt.show()


# Residual Plot
plt.figure(figsize=(width, height))
sns.residplot(df['highway-mpg'], df['price'])
plt.show()

# Multiple linear regression
Y_hat = lm.predict(Z)

plt.figure(figsize=(width, height))

ax1 = sns.distplot(df['price'], hist=False, color="r", label="Actual Value")
sns.distplot(Yhat, hist=False, color="b", label="Fitted Values" , ax=ax1)

plt.title('Actual vs Fitted Values for Price')
plt.xlabel('Price (in dollars)')
plt.ylabel('Proportion of Cars')

plt.show()
plt.close()

# polynominal plot
def PlotPolly(model, independent_variable, dependent_variabble, Name):
    x_new = np.linspace(15, 55, 100)
    y_new = model(x_new)

    plt.plot(independent_variable, dependent_variabble, '.', x_new, y_new, '-')
    plt.title('Polynomial Fit with Matplotlib for Price ~ Length')
    ax = plt.gca()
    ax.set_axis_bgcolor((0.898, 0.898, 0.898))
    fig = plt.gcf()
    plt.xlabel(Name)
    plt.ylabel('Price of Cars')

    plt.show()
    plt.close()


print("done")

x = df['highway-mpg']
y = df['price']
print("done")

# Here we use a polynomial of the 3rd order (cubic)
f = np.polyfit(x, y, 3)
p = np.poly1d(f)
print(p)

PlotPolly(p,x,y, 'highway-mpg')

np.polyfit(x, y, 3)